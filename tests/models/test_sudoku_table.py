import unittest
from src.models.sudoku_table import SudokuTable


DUMMY_TABLE_STR = "1 2 3 4 5 6 7 8 9\n" * 9


class SudokuTableTest(unittest.TestCase):
    def test_initialize_from_string(self):
        actual = SudokuTable.from_str(DUMMY_TABLE_STR)
        assert isinstance(actual, SudokuTable)

    def test_split_input_string_to_rows(self):
        actual = SudokuTable.input_string_to_rows(DUMMY_TABLE_STR)
        expected = [[1, 2, 3, 4, 5, 6, 7, 8, 9]] * 9

        assert actual == expected

    def test_get_columns(self):
        rows = [[1], [2], [3]] * 3
        table = SudokuTable(rows)

        actual = table.get_column(0)
        expected = [1, 2, 3, 1, 2, 3, 1, 2, 3]

        assert actual == expected

    def test_get_rows(self):
        rows = [[1], [2], [3]] * 3
        table = SudokuTable(rows)

        actual = table.get_row(0)
        expected = [1, 2, 3, 1, 2, 3, 1, 2, 3]

        assert actual == expected

    def test_get_box(self):
        rows = [[1, 2, 3], [2, 3, 4], [3, 4, 5]] * 3
        table = SudokuTable(rows)

        actual = table.get_box(0)
        expected = [1, 2, 3, 2, 3, 4, 3, 4, 5]

        assert actual == expected
