class SudokuTable:
    rows: [[int]]

    def __init__(self, rows):
        self.rows = rows

    @classmethod
    def from_str(cls, input_str):
        rows = cls.input_string_to_rows(input_str)
        return cls(rows)

    @staticmethod
    def input_string_to_rows(input_str):
        def is_empty_lambda(element): return bool(element)

        def string_to_int_list(row_string): return [
            int(cell) for cell in row_string.split()
        ]

        rows = filter(is_empty_lambda, input_str.split("\n"))

        return list(map(string_to_int_list, rows))

    def get_column(self, idx):
        return [row[idx] for row in self.rows]

    def get_row(self, idx):
        return self.rows[idx]

    def get_box(self, idx):
        column_offset = idx % 3
        row_offset = idx // 3

        box = [self.get_cell(row, col) for row in range(column_offset, column_offset+3)
               for col in range(row_offset, row_offset+3)]
        return box

    def get_cell(self, row_idx, col_idx):
        print(f"{row_idx=}, {col_idx=}")
        return self.rows[row_idx][col_idx]
