# Sudoku Solver

Just a short project to practice design patterns.

## Goals

- Practice writing small tests
- decoupling data structures from algorithms.
